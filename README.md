Native Dialogs
==============

The current format of the dialogs is:

```
<line_number> - <speech_registry>
[<language_tag>] <person_letter>) <text>
```

**Speech registry:** casual, normal and polite.

Example:

```
3 - Casual
[pt-PT] A) Não percebo nada de japonês.
[en-US] A) I don't understand japanese at all.
[jp-JP] A) 日本語は全然分からない。
```

Even if it's already translated into your native language, feel free to add
your own version.

For the time being, the main goal is to gather as many natural translations
as possible.
