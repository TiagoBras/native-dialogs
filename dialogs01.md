Dialogs - 会話 - 01
==================

### 1) Chegada a Londres - < titulo em japonês >



> **Meio de comunicação:** Telemóvel.

> **Contexto:** **B** vai a Londres visitar amigo **A**.

1
[pt-PT] A) Olá, então já chegaste a londres?
[jp] A) 〜日本語

2
[pt-PT] B) Ainda não. Houve um atraso, só agora é que vou entrar no avião.

3
[pt-PT] A) A sério?

4
[pt-PT] B) Sim. É verdade, espero por ti ou almoço logo?

5
[pt-PT] A) Não vai dar, só estou livre a partir das 2 e tal. Eu almoço qualquer coisa por aqui mesmo e depois vou ter contigo e damos um passeio pela zona de Liverpool Street.

6
[pt-PT] B) Tudo bem. Acho que vou ao Wasabi então. Se quiseres posso ir ter contigo a outra zona com metro por perto.

7
[pt-PT] A) Ah não, aquela área é fixe, e como tu nunca tiveste lá, calha mesmo bem.

8
[pt-PT] B) Fixe.

9
[pt-PT] A) Já foste ao pé da Torre de Londres?

10
[pt-PT] B) Acho que nunca passei ao lado.

11
[pt-PT] A) OK, então vamos lá também.

12
[pt-PT] B) Parece-me bem. O chato é que só temos à volta de 2 horas.

13
[pt-PT] A) É suficiente, acho, não te preocupes.

14
[pt-PT] B) Bem, vou ter de desligar o telefone. Até já!

15
[pt-PT] A) Até já. Boa viagem!

16
[pt-PT] ~ ** depois de aterrar e chegar à estação ** ~

17
[pt-PT] B) Já cheguei!

18
[pt-PT]~ ** depois de 5 minutos sem resposta ** ~

19
[pt-PT] A) Ah desculpa, estava no metro, não tinha rede. Onde estás?

20
[pt-PT] B) Estou na estação, não sei exactamente onde...

21
[pt-PT] A) Dentro ou fora?

22
[pt-PT] B) Dentro. Se quiseres posso ir para fora. Sabes onde é o Wasabi?

23
[pt-PT] A) Estás parado neste momento?

24
[pt-PT] B) Sim, no andar de cima (primeiro andar).

25
[pt-PT] A) Acho que já te vi. Estás a tirar fotos?

26
[pt-PT] B) Sim!
